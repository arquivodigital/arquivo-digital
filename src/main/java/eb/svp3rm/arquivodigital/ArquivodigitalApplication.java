package eb.svp3rm.arquivodigital;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArquivodigitalApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArquivodigitalApplication.class, args);
	}

}
